import {LitElement, html} from '@polymer/lit-element'

class LitRossi extends LitElement {
    _render() {
        return html`
            <h1>Loop Test 1</h1>
             <a href="https://www.dwitter.net/d/3697">Quelle</a> 
            <canvas id="draw"></canvas>
            
        `;
    }

    _firstRendered() {
        var t = 0
        var c = this.shadowRoot.getElementById('draw')
        c.width = 1920
        c.height = 1080
        var x = c.getContext('2d')

        setInterval(function() {
            c.width = 1920
            c.height = 1080
            t++
            x.scale(0.3,0.3)
            var w = c.width
            for(var i=w;i>=20;i-=10) {
                for (var j = 20; j < 1080; j += 10) {
                    var i2 = i - w / 2;
                    var j2 = j - 1080 / 2
                    var d = Math.sqrt(i2 * i2 + j2 * j2) / 10
                    var o = 9 + Math.cos((d - t * 7)) * 3
                    x.fillRect(i-o/2, j-o/2, o, o)
                }
            }

        }, 16)
    }
}

customElements.define('rossi-element', LitRossi)